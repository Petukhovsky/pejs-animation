/* =============================================================
     ===== CANVAS =====
     ===== animation ====
 script written by Petukhovsky
 http://petukhovsky.com


 v.0.3.4 (2022.06.13) 
 + размер экрана под телефон а также кнопку full screen для телефна Samsung, чтобы открывалось в новом окне
 + задержка роста деревьев (start time)

 v.0.3.5 (2022.06.15)
 + доделать снег - должен медленно снижаться раскачиваясь

 v.0.3.6 (2022.06.16)
 + сделать снег постепенно начинающимся

 v.0.3.7 (2022.06.19)
 + добавлен fps score
 =  2363, 2360 fps score. Тест сцены 30 сек до оптимизации дает на ASUS M7400Q 

 v.0.3.8 (2022.06.20)
 + снег ложится на землю под прямым углом

 v.0.3.9 (2022.06.22)
 + заменено название на посвященное годовщине
 + full screen mode

 v.0.4.0 (2022.07.25)
 + версия про ДР 32 года
 + сделать кнопку ран не вылетающую в сторону на мобильной верстке
 + поменять заголовок эксперименты на что-то красивое для жены
 + внедрить 32

  v.0.4.1 (2022.07.26)
+ фиолетовый фон
+ easy добавить пиксельную собаку на слой с деревьями
+ релиз



 TO DO
==========СДЕЛАТЬ ОТВЕТВЛЕНИЕ В НОВЫЙ ПРОЕКТ PEJS2 или что-то такое=============
 разработать функцию random которая работает внутри JS скрипта и опирается полностью на SEED
 чтобы SEED всегда давали одинакоые результаты

 ==проекты==
 Growing grass
 в этом проекте на заднем фоне медленно растет трава и деревья, трава может быть живой и колыхаться
 реализовать этот проект и использовать canvas как бекграуд или картинку, без всяких кнопок run

 как вариант проекта, анимация соединяющихся блокчейн узлов и т.п.
 
 Labirint game
 Лабиринт генерится на базе сида, игра на двоих, надо быстро из него выбраться, управление стрелками. 


 


 5 фон должен быть градиентом как буд-то восходит солнце
 6 суперсложность - по мере восхода солнца снег првращается в дождь
 7 добавить салюты - как деревья, только без буфера
 8 добавить зведы и при восходе они становятся невидны
 9 добавить собаку виляющую хвостом или ходящую 

 =============================================================
*/

"use strict"; //All my JavaScript written in Strict Mode http://ecma262-5.com/ELS5_HTML.htm#Annex_C

(function(){
    // ======== private vars ========
    var clientver = "v.0.4.0";
    var htmlid = "pejs-animation-3";

    var srvaddress = "";
    //var gameserver = srvaddress + "?";
    var graphicsdir = srvaddress + "img/";

    var fps = 0, fpsscore = 0; //Счётчик FPS
    var canvas, ctx; //Объекты canvas, context
    var canvasbuf; //Буфер для рисования фона
    var canvastreebuf; //Буфер для ранее нарисованных объектов
    var pixeldata;

    var contwidth = document.documentElement.scrollWidth - 10; //Ширина контейнера
    var contheight = document.documentElement.scrollHeight - 20; //Высота контейнера

    var contcolor = "#5d0080"; //Цвет заливки
    var rcontcolor = (("0x"+contcolor.slice(1)) & 0xff0000) >> 16; //R компонента в DEC от contcolor
    var x, y; //Переменные необходимые для построения дерева

    var ldu = 0, tpldu = 0; //time passed from last draw update
    var requestAnimFrame; //Функция анимации

    var appstatus = "load"; // Состояния приложения  -1 (load) - приложение грузится, 0 (startmenu) - загрузка прошла, 1(run) - работа, 2(menu) - меню, 3(over) - работа окончена

    var load = {point: 0, total: 3}; //всего загружаем 2 элемента и 1 для подключения

    var ui_font_color = "#6f6864"; //Цвет шрифта по умолчанию
    var ui_font_style = "9pt Verdana"; //([font style][font weight][font size][font face]).
    var ui_font_warning_color = "#ff0000"; //Красный

    var starttime = 0;
    var totalanimtime = 90000; //время сцены с мс

    var animtime =0;
    
    var trees = [
        {
            x:60,           //Смещение координаты X для отрисовки
            shortness:0.65, //Коэфициент уменьшения дерева
            ln:120,          //Длинна ствола
            minln:3,        //Длинна ветки при которой анимация прекращается 
            div1:2,         //Угол роста ветки.
            div2:6,         //Сила развития дерева вправо если поставить 4 и без мутаций - будет классическое дерево
            div3:4,         //Сила развития дерева влево если поставить 4 и без мутаций - будет классическое дерево
            rnd1:0.10,      //Рандомная мутация длины
            rnd2:0.12,      //Рандомная мутация угла
            starttime:0,    //Задержка начла рисования дерева в милисекундах
            treecolor:'#f62', 
            linespeed:48,   //скорость роста дерева 
            objcts:[]
        }, 
        {x:100, shortness:0.65, ln:154, minln:3, div1:2, div2:8, div3:4, rnd1:0.07, rnd2:0.08, starttime:2000, treecolor:'#fb0', linespeed:10, objcts:[]},
        {x:140, shortness:0.68, ln:104, minln:3, div1:2, div2:5, div3:7, rnd1:0.02, rnd2:0.1, starttime:0, treecolor:'#d10', linespeed:30, objcts:[]},
        {x:180, shortness:0.72, ln:80, minln:4, div1:2, div2:5, div3:6, rnd1:0.07, rnd2:0.15, starttime:0, treecolor:'#f60', linespeed:35, objcts:[]},
        {x:230, shortness:0.65, ln:180, minln:3, div1:2, div2:7, div3:9, rnd1:0.02, rnd2:0.15, starttime:0, treecolor:'#b32',linespeed:30,objcts:[]},
        {x:240, shortness:0.65, ln:120, minln:2, div1:2, div2:4, div3:8, rnd1:0.02, rnd2:0.1, starttime:0, treecolor:'#f53', linespeed:15,objcts:[]},
        {x:380, shortness:0.65, ln:137, minln:3, div1:2, div2:6, div3:8, rnd1:0.25, rnd2:0.1, starttime:0, treecolor:'#f70', linespeed:20,objcts:[]},
        {x:430, shortness:0.75, ln:120, minln:5, div1:2, div2:10, div3:10, rnd1:0.02, rnd2:0.1, starttime:0, treecolor:'#f62', linespeed:22,objcts:[]},
        {x:820, shortness:0.60, ln:100, minln:4, div1:2, div2:7, div3:5, rnd1:0.1, rnd2:0.1, starttime:4000, treecolor:'#e42', linespeed:27,objcts:[]},
        {x:820, shortness:0.60, ln:100, minln:4, div1:2, div2:6, div3:6, rnd1:0.1, rnd2:0.1, starttime:4000, treecolor:'#e42', linespeed:27,objcts:[]}
    ];//Массив объектов составляющих дерево

    var snows_amount = 12000;
    var totalsnowtime = 60000;
    var snows = [];//Массив объектов составляющих снег

    var imgbase, imganim, imgpart; //Изображения

    var logo = {type: "img", x: contwidth/2-200, y: 250, ox: 604, oy: 23, w: 468, h: 55};
    var logotext = {type: "text", x: 300, y: 150, text: ""};
    var logo32 = {type: "img", x: contwidth/2-105, y: 250, ox: 624, oy: 78, w: 210, h: 175};
    var logodog = {type: "img", x: contwidth/2+105, y: contheight-145, ox: 654, oy: 254, w: 110, h: 145};

    var fpsobject = {type: "text", x: 16, y: 16, text: "FPS: "};

    ////////////////////////////////////////////////////////////////////////////
    var init = function () {
        load.point = 0;
        load.total = 3; //всего загружаем 2 элемента + 1 статус что всё ОК

        document.getElementById(htmlid+"-div").style.display = "none";
        document.getElementById(htmlid+"-run").disabled = true;
        document.getElementById(htmlid+"-run").onclick = animation_run;

        console.log("init function");

        // ---- init script ----
        canvas = document.getElementById(htmlid + "-canvas"); //Хватаем объект
        canvas.width = contwidth; //Задаём ширину холста
        canvas.height = contheight;  //Задаём ширину холста
        ctx = canvas.getContext("2d"); //Инициализируем работу с контекстом. Контекст позволяет работать с canvas применяя методы рисования

        // Если ничего нет - возвращаем обычный таймер
        requestAnimFrame = window.requestAnimationFrame ||
            window.webkitRequestAnimationFrame ||
            window.mozRequestAnimationFrame ||
            window.oRequestAnimationFrame ||
            window.msRequestAnimationFrame ||
            function (callback) {
                window.setTimeout(callback, 1000 / 60);
            };

        // ---- fps count ----
        setInterval(function () {
            fpsobject.text = "FPS: " + (fps * 2);
            fpsscore += fps;
            fps = 0;

        }, 500); // update every 1/2 seconds

        // ---- screen size ----
        resize();
        window.addEventListener("resize", resize, false);

        console.log("init OK");
        
        
        load_app(); //Загружаем ресурсы и т.п.
    };


    var resize = function () { //Не используется
        // ---- screen resize ----
        //nx = findPosX(canvas);
        //ny = findPosY(canvas);
    };


//////////////////////////////ANIMATION LOGIC/////////////////////////////////////

    var animation_run = function () {
        if (appstatus != "startmenu") return; //Загрузка не прошла или мы по середине игры то не стартуем
        appstatus = "run";

        starttime = (new Date()).getTime();

        document.getElementById(htmlid+"-div").style.display = "none"; //Скрываем элементы управления

        // Создаём буфер нужного размера:
        canvasbuf = document.createElement("canvas");
        canvasbuf.width = contwidth;
        canvasbuf.height = contheight;
        canvasbuf.ctx = canvasbuf.getContext("2d");

        canvasbuf.ctx.fillStyle = contcolor; // цвет фона
        canvasbuf.ctx.strokeStyle = '#020'; //цвет линий
        canvasbuf.ctx.fillRect(0, 0, canvas.width, canvas.height);
        canvasbuf.ctx.lineWidth = 2; // ширина линий

        //Создаём буфер нужного размера для готовых частей дерева
        canvastreebuf = document.createElement("canvas");
        canvastreebuf.width = contwidth;
        canvastreebuf.height = contheight;
        canvastreebuf.ctx = canvastreebuf.getContext("2d");

        canvastreebuf.ctx.fillStyle = contcolor; // цвет фона
        canvastreebuf.ctx.strokeStyle = '#020'; //цвет линий
        canvastreebuf.ctx.lineWidth = 2; // ширина линий
        canvastreebuf.ctx.fillRect(0, 0, canvas.width, canvas.height); 

        //Вывод 32
        drawobj(logo32, canvastreebuf.ctx);

        //Вывод собаки
        drawobj(logodog, canvastreebuf.ctx);

        //Генерация деревьев
        for(let n=0; n<trees.length; n++){
            trees[n].objcts = [];
            x = trees[n].x; //центр дерева с отступом
            y = canvas.height;	// положение ствола (координаты в канвас идут сверху вниз)
            createTree(n, x, y, trees[n].ln,  trees[n].minln, Math.PI / trees[n].div1); //рисование линий дерева (начало x, начало y, длинна линии, минимальная длинна, делитель)
            trees[n].objcts[0].starttime = trees[n].starttime; //передача задержки рисования первой ветки
        }

        let k = totalanimtime/snows_amount; //сколько мс времени есть на 1 снежинку
        //Генерация снега
        for(let n=0; n<snows_amount; n++){
            snows[n] = {
                lastx:Math.random()*canvas.width, //Точка изменеия x скорости                          
                x:0,                          //координата x
                lasty:0,                      //начальная координата y
                y:0,                          //координата y
                col:'#ded',                   //цвет
                vy:70*Math.random()+30,       //вертикальная скорость
                vx:100*Math.random()-50,      //горизонтальная скорость  
                ttochangevx: 2000*Math.random()+500,   //время через которое меняется направление гор скорости
                lastchangevxtime:0,
                drwtime:0,
                xsize: random(-1,2),
                ysize: 1, 
                starttime: (totalsnowtime-n*k)*Math.random()+n*k, //Задержка начла рисования дерева в милисекундах
                active:true 
            };

            //выравниваем плотность снега после первой половины сцены
            if(n>snows_amount/2) snows[n].starttime = (totalanimtime/2)*Math.random()+totalanimtime/2;

        }        

        processloop();

        ldu = (new Date()).getTime(); //last draw update
        animloop();
    };

    var load_app = function () {

        imgbase = new Image();	// Создание нового объекта изображения
        imgbase.src = graphicsdir + "base.png";	// Путь к изображениям
        imgbase.onload = drawloading;   // Событие onLoad, ждём момента пока загрузится изображение

        /*
        imganim = new Image();
        imganim.src = graphicsdir + "anim.png";
        imganim.onload = drawloading;   // Событие onLoad, ждём момента пока загрузится изображение

        imgpart = new Image();
        imgpart.src = graphicsdir + "part.png";
        imgpart.onload = drawloading;
        */


        drawloading();
        drawloading();

    };

    
    var run_startmenu = function () { //Выполняем по завершении загрузки

        draw_welcome();
        appstatus = "startmenu"; //Загрузка OK

        document.getElementById(htmlid+"-div").style.display = "inline"; //Показываем блок с кнопкой
        document.getElementById(htmlid+"-run").disabled = false; //Активируем кнопку
    };


    ////////////////////////////////////////////////////////////////////////////
    var drawloading = function () { //Функция отображения загрузки и также учета прогресса загрузки
        load.point++;

        ctx.fillStyle = contcolor; //Заливаем цветом по умолчанию
        ctx.fillRect(0, 0, canvas.width, canvas.height); //Закрашивание экрана

        ctx.fillStyle = ui_font_color;

        //Рисование прогресса загрузки
        ctx.strokeRect(Math.floor(contwidth / 4), Math.floor(contheight / 2 - 6), Math.floor(contwidth / 2), 12);
        ctx.fillRect(Math.floor(contwidth / 4 + 1), Math.floor(contheight / 2 - 4), Math.floor(contwidth / 2 / load.total * load.point), 8);

        if (load.point >= load.total) {//Загрузка успешно завершена
             run_startmenu();
        }
    };

    var draw_scene = function () {

        tpldu = (new Date()).getTime() - ldu; //time passed last draw update
        ldu = (new Date()).getTime();
        fps++;

        animtime = starttime - ldu;

        if (appstatus == "run") {
            //canvasbuf.ctx.clearRect(0, 0, canvas.width, canvas.height); //Очищаем буфер, но в этом нет нужды, так как его перезаписываем буфером с деревьями
            canvasbuf.ctx.drawImage(canvastreebuf, 0, 0); //Помещаем в качестве подложки ранее отрисованные элементы деревьев                 
            canvasbuf.ctx.lineWidth = 2;
            canvastreebuf.ctx.lineWidth = 2;

            for(let n=0; n<trees.length; n++){ //Рисование всех веток

                canvasbuf.ctx.strokeStyle =  trees[n].treecolor;
                canvastreebuf.ctx.strokeStyle =  trees[n].treecolor;

                canvasbuf.ctx.beginPath(); //начало рисования линии 
                canvastreebuf.ctx.beginPath(); //рисуем в буфер содержащий готовые части дерева

                var tree = trees[n].objcts;
         
                for(let i=0; i<tree.length; i++){ //Отрисовываем все деревья
                                                        
                    if(tree[i].active) { //если ветка еще не отрисована до конца    
                        
                        tree[i].drwtime+=tpldu; //смотрим сколько время затрачено на её отрисовку    
                        
                        if(typeof tree[i].starttime!== "undefined" && tree[i].starttime>tree[i].drwtime) { //задержка на рисование дерева
                            tree[i].starttime-=tree[i].drwtime;
                            tree[i].drwtime = 0;
                        }


                        if(tree[i].drwtime > tree[i].ln*1000/trees[n].linespeed ) { //Если прошло достаточно времени на отрисовку
                            
                            canvasbuf.ctx.moveTo(tree[i].x1, tree[i].y1);
                            canvasbuf.ctx.lineTo(tree[i].x2, tree[i].y2);
                            
                            canvastreebuf.ctx.moveTo(tree[i].x1, tree[i].y1);//Рисуем в буфер полностью нарисованные ветки, чтобы больше не перерисовывать
                            canvastreebuf.ctx.lineTo(tree[i].x2, tree[i].y2);

                            for(let j=0; j<tree[i].child.length; j++){
                                tree[tree[i].child[j]].active = true;
                            }                                                       
                            tree[i].active = false; //эту ветку больше не рисуем

                        } else { //Если ветку надо отрисовать частично
                            canvasbuf.ctx.moveTo(tree[i].x1, tree[i].y1);
                            var x3 = tree[i].x1 + (tree[i].x2-tree[i].x1)/tree[i].ln * tree[i].drwtime/1000*trees[n].linespeed;
                            var y3 = tree[i].y1 + (tree[i].y2-tree[i].y1)/tree[i].ln * tree[i].drwtime/1000*trees[n].linespeed;
                            canvasbuf.ctx.lineTo(x3, y3);                           
                        }
                        
                    }

                }

                canvasbuf.ctx.stroke(); //конеч рисования линии дерева
                canvastreebuf.ctx.stroke();  

            }

            canvasbuf.ctx.lineWidth = 3;
            canvastreebuf.ctx.lineWidth = 3;
            pixeldata = canvastreebuf.ctx.getImageData(0, 0, contwidth, contheight).data;

            for(let n=0; n<snows.length; n++){ 
                if(snows[n].active){ //если снежинка еще падает и не упала на дно               
                    snows[n].drwtime += tpldu; //время которое снежинка находится в отрисовке    
                    snows[n].lastchangevxtime += tpldu; //время которое снежинка не меняла направления

                    if(typeof snows[n].starttime!== "undefined" && snows[n].starttime>snows[n].drwtime) { //задержка на рисование
                        snows[n].starttime -= snows[n].drwtime;
                        snows[n].drwtime = snows[n].lastchangevxtime = 0;
                        continue;
                    }

                    snows[n].y = snows[n].lasty + snows[n].drwtime/1000*snows[n].vy; //прибавление вертикальной скорости    
                    snows[n].x = snows[n].lastx + snows[n].lastchangevxtime/1000*snows[n].vx;

                    if(snows[n].y > canvas.height){
                        snows[n].active = false;
                        continue;
                    }
                    
                    if(snows[n].lastchangevxtime>snows[n].ttochangevx){ //меняем траекторию раз в ttochangevx
                        snows[n].lastchangevxtime = 0;
                        snows[n].lastx = snows[n].x; 
                        snows[n].vx = 100*Math.random()-50;
                    }

                    canvasbuf.ctx.strokeStyle =  snows[n].col;
                    canvasbuf.ctx.beginPath(); //начало рисования линии 
                    
                    canvasbuf.ctx.moveTo(snows[n].x, snows[n].y);
                    canvasbuf.ctx.lineTo(snows[n].x+snows[n].xsize, snows[n].y+snows[n].ysize);

                    canvasbuf.ctx.stroke(); //конеч рисования 

                    let realx = snows[n].x|0;
                    let realy = snows[n].y|0+1;

                    //Проверка, если по пути следования снега, есть препятствие то переводим снежинку в буфер и убираем из массива трисовки
                    //TODO удалять снежинку из массива, чтобы оптимизировать алгоритм
                    if(pixeldata[(realx + realy*contwidth) * 4] != rcontcolor || realy+1 == contheight){
                        canvastreebuf.ctx.strokeStyle =  snows[n].col;
                        canvastreebuf.ctx.beginPath(); //начало рисования линии 
                        
                        canvastreebuf.ctx.moveTo(snows[n].x, snows[n].y);
                        canvastreebuf.ctx.lineTo(snows[n].x+snows[n].xsize, snows[n].y+snows[n].ysize);
                        
                        canvastreebuf.ctx.stroke(); //конеч рисования                        
                        snows[n].active = false;
                    }


                }
            }

            


            //Копирование отрендереного кадра из буфера
            ctx.drawImage(canvasbuf, 0, 0);

            ctx.fillStyle = ui_font_color; //Устанавливаем цвет шрифта
            ctx.font = ui_font_style;      //Устанавливаем стиль шрифта
            ctx.textBaseline = "top";      //Текст выравнивается по верхнему углу

            //Вывод информации об игроке и его местоположении
            ctx.textAlign = "left";
            //drawobj(ui_player_text);

            //Вывод информации о времени ответа, версии клиента и прочем
            drawobj(fpsobject);

            //Вывод времени и количества кристаллов
            //drawtime();
            //drawcrystals();

            // Рисуем игровой мир
            /*
            for (var i = 0; i < activeobjects.length; i++) objects[activeobjects[i]].draw();
            var temparray = [];
            for (var i = 0; i < activeobjects.length; i++) if (deleteid[activeobjects[i]] != true) temparray.push(activeobjects[i]);
            activeobjects = temparray;
            deleteid = {};
            */
        }

        ctx.restore();
    };

    var draw_welcome = function () {
        ctx.fillStyle = contcolor; //Заливаем цветом по умолчанию
        ctx.fillRect(0, 0, canvas.width, canvas.height); //Закрашивание экрана

        //Вывод welcome
        drawobj(logo);

        ctx.textAlign = "left";
        drawobj(logotext);
    };

    var drawobj = function (_obj, _ctx=ctx) { //Рисоваие объекта по собственным координатам

        //Вывод объекта
        if (_obj.type == "img") {
            _ctx.drawImage(imgbase, //картинка
                _obj.ox, _obj.oy,
                _obj.w, _obj.h,
                _obj.x, _obj.y, //X,Y
                _obj.w, _obj.h
            );
        } else if (_obj.type == "text") {
            _ctx.save();
            if (undef(_obj.color)) _ctx.fillStyle = ui_font_color;
            else _ctx.fillStyle = _obj.color;

            if (undef(_obj.style)) _ctx.font = ui_font_style;
            else _ctx.font = _obj.style;

            _ctx.fillText(_obj.text, _obj.x, _obj.y);
            _ctx.restore();
        }
    };

    var drawobjc = function (_obj, _x, _y) { //Рисоваие объекта по координатам из параметров
        _obj.x = _x;
        _obj.y = _y;
        drawobj(_obj);
    };

    var drawobjx = function (_obj, _x) {
        _obj.x = _x;
        drawobj(_obj);
    };
    var drawobjy = function (_obj, _y) {
        _obj.y = _y;
        drawobj(_obj);
    };

    //------MATH DRAWING---------------------------
    // Функция обсчитывает дерево
    var createTree = function (n, x, y, ln, minLn, angle, par_id=-1) { //Создание объекта n-ного дерева
        if (ln > minLn) { //Если длинна ветки еще остается большой, то продолжаем цикл
            ln = ln * trees[n].shortness;
            
            let x2 = Math.round(x + ln * Math.cos(angle));
            let y2 = Math.round(y - ln * Math.sin(angle));

            let current_id = trees[n].objcts.push({x1:x, y1:y, x2:x2, y2:y2, angle:angle, ln:ln, active: false, parent: par_id, child:[], drwtime:0}) - 1; //пока ничего не рисуем, только добавляем в очередь заданий

            if (par_id!=-1){ //Если это не первая ветка
                trees[n].objcts[par_id].child.push(current_id); //Добавляем в родителя индекс потомка
            }else{
                trees[n].objcts[current_id].active = true;
            }

            x = x2;
            y = y2;

            //Дефект длинны листа и угла
            var lndefects = [];
            var angledefects = [];
            for (var m=0; m<2; m++){
                lndefects[m] = 0;
                angledefects[m] = 0;

                if(Math.random()<trees[n].rnd1){ //Дефект длины листа
                    if(Math.random()<0.5){
                        lndefects[m] = -ln * Math.random();
                    } else {
                        lndefects[m] = ln * Math.random();
                    }
                }

                if(Math.random()<trees[n].rnd2){ //Дефект угла
                    if(Math.random()<0.5){
                        angledefects[m] = 1;
                    } else {
                        angledefects[m] = -1;
                    }
                }
            }
          
            createTree(n, x, y, ln + lndefects[0], minLn, angle + Math.PI / (trees[n].div2 + angledefects[0]), current_id);
            createTree(n, x, y, ln + lndefects[1], minLn, angle - Math.PI / (trees[n].div3 + angledefects[1]), current_id);
            // если поставить угол Math.PI/4 , то выйдет классическое дерево
        }
    };


    ///////////////////////////////////TOOLS///////////////////////////////////////
    var random = function (min, max) {
        //min = Math.ceil(min);
        //max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min; //Максимум не включается, минимум включается
    }

    /*
    var xabs = function (_x) {
        if (_x > -2 && _x < 11) return _x;
        if (_x + mapsizex > -2 && _x + mapsizex < 11) return _x + mapsizex;
        return _x - mapsizex;
    }

    var yabs = function (_y) {
        if (_y > -2 && _y < 11) return _y;
        if (_y + mapsizey > -2 && _y + mapsizey < 11) return _y + mapsizey;
        return _y - mapsizey;
    }

    var inv_button_active = function () {
        var count = 0;
        for (var i = 0; i < inventory.size; i++) {
            if (def(inventory.obj[i]) && inventory.obj[i].type == this.cond1) count += inventory.obj[i].num;
        }
        return count >= this.cond2;
    }

    var obj_button_active = function () {
        var count = 0;
        for (var i = 0; i < objinv.len; i++) {
            if (def(objinv.obj[i]) && objinv.obj[i].type == this.cond1) count += objinv.obj[i].num;
        }
        return count >= this.cond2;
    }

    var clone = function (obj) {
        if (obj == null || typeof(obj) != "object")
            return obj;

        var temp = obj.constructor();

        for (var key in obj)
            temp[key] = clone(obj[key]);
        return temp;
    }

    */

    var undef = function (obj) {
        return typeof obj === "undefined";
    };

    var def = function (obj) {
        return !undef(obj);
    };

    //////////////////////////////MAIN LOOP/////////////////////////////////////
    var processloop = function () {
        if (appstatus != "run" && appstatus != "menu") return; //Не выполнять, если приложение не запущено

        if ((new Date()).getTime() > starttime + totalanimtime ) { //Анимация завершена
            appstatus = "startmenu";
            console.log("ANIMATION IS OVER, FPS SCORE = "+fpsscore);         
            setTimeout(run_startmenu, 100);
        }

        if (appstatus == "run" || appstatus == "menu") setTimeout(processloop, 500);
    };

    var animloop = function () {
        draw_scene();
        if (appstatus == "run" || appstatus == "menu") {
            requestAnimFrame(animloop);
        }
    };

    return {
        ////////////////////////////////////////////////////////////////////////////
        // ---- onload event ----
        load: function () {
            console.log("load function");
            window.addEventListener("load", function () {
                init();
            }, false);
        }
    }
})().load();

